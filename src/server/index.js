const csvtojson = require("csvtojson");
const fs = require("fs");

const pathToMatchCsv = "../data/matches.csv";
const pathToDeliveryCsv = "../data/deliveries.csv";

const iplFunction = require('./ipl');
const propelIplFunction = require('./propel_ipl');


csvtojson().fromFile(pathToMatchCsv).then(matchSource => {


    csvtojson().fromFile(pathToDeliveryCsv).then(deliverySource => {

        //Number of matches played per year for all the years in IPL
        iplFunction.NumberOfMatchesPlayed(matchSource)

        //Number of matches won per team per year in IPL.
        iplFunction.NumberOfMatchesWon(matchSource)

        // Extra runs conceded per team in the year 2016
        iplFunction.extraRunsConceded(matchSource, deliverySource, 2016);

        //Top 10 economical bowlers in the year 2015
        iplFunction.topEconomicalBowler(matchSource, deliverySource, 2015)

        //propel
        //Find the number of times each team won the toss and also won the match
        propelIplFunction.teamWonTheToss(matchSource)

        //Find a player who has won the highest number of Player of the Match awards for each season
        propelIplFunction.manOfTheMatchPlayer(matchSource)

        //Find the strike rate of a batsman for each season
        propelIplFunction.strikeRateOfBatsman('V Kohli', matchSource, deliverySource)

        //Find the highest number of times one player has been dismissed by another player
        propelIplFunction.findHighestDismissed(deliverySource)

        //Find the bowler with the best economy in super overs
        propelIplFunction.bestEconomyInSuperOvers


    })
})