const fs = require("fs");
/**
 * Find the number of times each team won the toss and also won the match
 * @param {*} source :source value from matches.csv file
 */

function teamWonTheToss(source) {
    const tossWinner = {}
    source.map(matchSourceValue => {
        if (matchSourceValue.toss_winner == matchSourceValue.winner) {
            if (tossWinner[matchSourceValue.toss_winner] !== undefined) {
                tossWinner[matchSourceValue.toss_winner] += 1
            }
            else {
                tossWinner[matchSourceValue.toss_winner] = 1
            }

        }
    })
    //tossWinner is storing the number of times each team won the toss and also won the match
    fs.writeFile("../output/wonTosswonMatch-PS-1.json", JSON.stringify(tossWinner), function (err) {
        console.log(err);

    });
}

/**
 * Find a player who has won the highest number of Player of the Match awards for each season
 * @param {*} source source value from matches.csv file
 */

function manOfTheMatchPlayer(source) {
    const manOfTheMatch = {}
    source.map(matchSourceValue => {
        if (manOfTheMatch[matchSourceValue.season] !== undefined) {
            if (manOfTheMatch[matchSourceValue.season][matchSourceValue.player_of_match]) {
                manOfTheMatch[matchSourceValue.season][matchSourceValue.player_of_match] += 1;
            }
            else {
                manOfTheMatch[matchSourceValue.season][matchSourceValue.player_of_match] = 1;
            }
        }

        else {
            manOfTheMatch[matchSourceValue.season] = {}
            manOfTheMatch[matchSourceValue.season][matchSourceValue.player_of_match] = 1;
        }
    })
    const matchAwardPlayer = {};
    for (let outKeys in manOfTheMatch) {
        let newArray = []
        for (let inKeys in manOfTheMatch[outKeys]) {
            newArray.push(manOfTheMatch[outKeys][inKeys]);
        }

        let max = Math.max.apply(null, newArray);

        for (let inKeys in manOfTheMatch[outKeys]) {
            if (manOfTheMatch[outKeys][inKeys] == max) {
                matchAwardPlayer[outKeys] = inKeys;
            }
        }

    }
    // matchAwardPlayer is storing the player who has won the highest number of Player of the Match awards for each season
    fs.writeFile("../output/wonHighestNumberofMatch-PS-2.json", JSON.stringify(matchAwardPlayer), function (err) {
        console.log(err);

    });
}

/**
 * Find the strike rate of a batsman for each season
 * @param {*} givenBatsMan 
 * @param {*} source source value from matches.csv file
 * @param {*} devSource source value from deliveries.csv file
 */

function strikeRateOfBatsman(givenBatsMan, source, devSource) {
    let year;
    let givenBatsManStrike = givenBatsMan;
    givenBatsManStrike = {};
    devSource.map(sourceValue => {
        if (sourceValue.batsman == givenBatsMan) {
            year = source.find(matchSourceValue => {
                if (matchSourceValue.id == sourceValue.match_id) {
                    return matchSourceValue.season;
                }
            })

            if (givenBatsManStrike[year] !== undefined) {
                givenBatsManStrike[year][0] += parseInt(sourceValue.total_runs, 10);
                givenBatsManStrike[year][1] += 1
            }
            else {
                givenBatsManStrike[year] = [];
                givenBatsManStrike[year].push(parseInt(sourceValue.total_runs, 10), 1);
            }
        }
    })
    let finalObject;
    finalObject = (((givenBatsManStrike[year][0]) / (givenBatsManStrike[year][1])) * 100);
    //finalObject is storing the value of the strike rate of a batsman for each season
    fs.writeFile("../output/strikeRateOfBatsman-PS-3.json", JSON.stringify(finalObject), function (err) {
        console.log(err);
    });
}
/**
 * Find the highest number of times one player has been dismissed by another player
 * @param {*} devSource :source value from deliveries.csv file
 */

function findHighestDismissed(devSource) {
    let newArray = []
    devSource.find(sourceValue => {
        if (sourceValue.player_dismissed != '') {

            /*This checking function is used for to check the value of newArray.
            *if the combination of batsman and bowler is matched,
            *its return the index+1 value otherwise return false
            */
            function checkingArray(arr, batman, bowler) {
                for (let index = 0; index < arr.length; index++) {
                    if (arr[index][0] == batman && arr[index][1] == bowler) {
                        return index + 1;
                    }
                } return false;
            }
            if (checkingArray(newArray, sourceValue.batsman, sourceValue.bowler)) {
                let index = checkingArray(newArray, sourceValue.batsman, sourceValue.bowler) - 1;
                newArray[index][2] += 1
            }
            else {
                newArray.push([sourceValue.batsman, sourceValue.bowler, 1]);
            }
        }
    })
    newArray.sort((a, b) => { return b[2] - a[2] })
    //[0] index of the array newArray is storing highest number of times one player has been dismissed by another player

    fs.writeFile("../output/highestNumberDismissed-PS-4.json", JSON.stringify(newArray[0]), function (err) {
        console.log(err);

    });
}

/**
 * Find the bowler with the best economy in super overs
 * @param {*} devSource :source value from deliveries.csv file
 */

function bestEconomyInSuperOvers(devSource) {
    const myObj = {}
    devSource.find(sourceValue => {
        if (sourceValue.is_super_over == '1') {
            if (myObj[sourceValue.bowler] !== undefined) {
                myObj[sourceValue.bowler][0] += parseInt(sourceValue.total_runs, 10);
                myObj[sourceValue.bowler][1] += 1;
            }
            else {
                myObj[sourceValue.bowler] = [];
                myObj[sourceValue.bowler].push(parseInt(sourceValue.total_runs, 10), 1);
            }
        }
    })
    const newArray = [];
    for (let key in myObj) {
        let val = ((myObj[key][0]) * 6) / (myObj[key][1]);
        newArray.push([key, val]);
    }
    newArray.sort((a, b) => { return a[1] - b[1] });
    // [0][0] index of the array newArray is storing the bowler with the best economy in super overs

    let bestEconomy = newArray[0][0];

    fs.writeFile("../output/bowlerWithBestEconomy-PS-5.json", JSON.stringify(bestEconomy), function (err) {
        console.log(err);

    });
}



module.exports = {
    teamWonTheToss,
    manOfTheMatchPlayer,
    strikeRateOfBatsman,
    findHighestDismissed,
    bestEconomyInSuperOvers,
}
