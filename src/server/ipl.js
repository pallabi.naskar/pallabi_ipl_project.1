const fs = require("fs");

/**
 * Number of matches played per year for all the years in IPL
 * @param {*} source : source value from matches.csv file
 */

function NumberOfMatchesPlayed(source) {
    const newObject = {};
    source.map(matchSourceValue => {
        if (newObject[matchSourceValue.season] !== undefined) {
            newObject[matchSourceValue.season] += 1;
        }
        else {
            newObject[matchSourceValue.season] = 1;
        }
    })
    //newObject is storing matches played per year for all the years in IPL
    fs.writeFile("../output/matchesPerYear-1.json", JSON.stringify(newObject), function (err) {
        console.log(err);
    });
}
/**
 * Number of matches won per team per year in IPL
 * @param {*} source :source value from matches.csv file
 */

function NumberOfMatchesWon(source) {
    const wonObject = {};
    source.map(matchSourceValue => {
        if (wonObject[matchSourceValue.season] !== undefined) {
            if (wonObject[matchSourceValue.season][matchSourceValue.winner]) {
                wonObject[matchSourceValue.season][matchSourceValue.winner] += 1;
            }
            else {
                wonObject[matchSourceValue.season][matchSourceValue.winner] = 1;
            }
        }

        else {
            wonObject[matchSourceValue.season] = {};
            wonObject[matchSourceValue.season][matchSourceValue.winner] = 1;
        }

    })
    //wonObject is storing number of matches won per team per year in IPL
    fs.writeFile("../output/matchesWonPerTeam-2.json", JSON.stringify(wonObject), function (err) {
        console.log(err);
    });

}

/**
 * Extra runs conceded per team in the year 2016
 * @param {*} source source value from matches.csv file
 * @param {*} devSource source value from deliveries.csv file
 */

function extraRunsConceded(source, devSource, year) {
    const extraRun = {};
    source.map(matchSourceValue => {
        if (matchSourceValue.season == year) {
            devSource.find(sourceValue => {
                if (sourceValue.match_id == matchSourceValue.id) {
                    let val = parseInt((sourceValue.extra_runs), 10);

                    if (extraRun[sourceValue.bowling_team] !== undefined) {
                        extraRun[sourceValue.bowling_team] += val;
                    }
                    else {
                        extraRun[sourceValue.bowling_team] = val;
                    }
                }
            })
        }
    })
    //extraRun is storing extra runs conceded per team in a given year
    fs.writeFile("../output/extraRunsConcede-3.json", JSON.stringify(extraRun), function (err) {
        console.log(err);
    });
}
/**
 * Top 10 economical bowlers in the year 2015
 * @param {*} source source value from matches.csv file
 * @param {*} devSource source value from deliveries.csv file
 */

function topEconomicalBowler(source, devSource, year) {
    const economyBowler = {};
    source.map(matchSourceValue => {
        if (matchSourceValue.season == year) {
            devSource.find(sourceValue => {
                if (sourceValue.match_id == matchSourceValue.id) {
                    if (economyBowler[sourceValue.bowler] !== undefined) {
                        economyBowler[sourceValue.bowler][0] += parseInt(sourceValue.total_runs, 10);
                        economyBowler[sourceValue.bowler][1] += 1;
                    }
                    else {
                        economyBowler[sourceValue.bowler] = [];
                        economyBowler[sourceValue.bowler].push(parseInt(sourceValue.total_runs, 10), 1);
                    }
                }
            })

        }
    })

    let myArr = [];
    for (let key in economyBowler) {
        let value = (economyBowler[key][0]) * 6 / (economyBowler[key][1]);
        myArr.push([key, value]);
    }
    myArr.sort(function (a, b) {
        return a[1] - b[1];
    })
    let i = 0;
    const finalObj = {};
    while (i < 10) {
        finalObj[myArr[i][0]] = myArr[i][1];
        i++;
    }
    //finalObj is storing Top 10 economical bowlers in a given year
    fs.writeFile("../output/top10EconomicalBowler-4.json", JSON.stringify(finalObj), function (err) {
        console.log(err);
    });
}


module.exports = {
    NumberOfMatchesPlayed,
    NumberOfMatchesWon,
    extraRunsConceded,
    topEconomicalBowler,

};

